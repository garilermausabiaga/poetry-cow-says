---

Below 👇 are the sections that the README.md must have.
For guidelines around developing gear using this repository template
refer to [GUIDELINES.md](GUIDELINES.md).

---

# Cowsay Gear

Implementation of cowsay.
<!-- markdownlint-disable-file MD040 -->
```
  ______________
< Flywheel rocks >
  ==============
            \
             \
               ^__^                             
               (oo)\_______                   
               (__)\       )\/\             
                   ||----w |           
                   ||     ||  
```

## Usage

### Inputs

* __text-file__: A text file containing text to say.

### Configuration

* __debug__ (boolean, default False): Include debug statements in output.
* __animal__ (string, default 'cow'): Animal to say text.

## Contributing

For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
